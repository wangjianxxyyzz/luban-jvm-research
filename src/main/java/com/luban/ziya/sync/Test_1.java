package com.luban.ziya.sync;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.TimeUnit;

public class Test_1 {

    public static void main(String[] args) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);

        Test_1 obj = new Test_1();

        System.out.println(ClassLayout.parseInstance(obj).toPrintable());

        synchronized (obj) {
            System.out.println(ClassLayout.parseInstance(obj).toPrintable());
        }
    }
}
